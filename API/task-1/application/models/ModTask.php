<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModTask extends CI_Model {

	public function __construct()  
	{  
		 // Call the Model constructor  
		parent::__construct();  
		$this->load->database('default');
	}  

	public function Loginn($data)
	{
		$username = $data['username'];
		$password = $data['password'];

		$query = $this->db->query("SELECT * from users where username = '$username' and flag=1");
		$user_details = $query->result_array(); 
		if($this->db->affected_rows() == '1')
		{
			$user_pass = $user_details[0]['password'];
			$password_salt = $user_details[0]['salt'];
			$hexsalt = strtoupper(bin2hex($password_salt));
			$hash = hash('sha256', "$password" . "$hexsalt");
			if($hash == bin2hex($user_pass)) 
			{
				$result["statusCode"] = "200";
				$result["status"] = 'success';
				$result["msg"] = "Login Success";
			}
			else {
				$result["statusCode"] = "400";
				$result["status"] = 'fail';
				$result["msg"] = "Password is Incorrect";
			}
		} 
		else {
				$result["statusCode"] = "400";
				$result["status"] = 'fail';
				$result["msg"] = "Specified User Not Found. Dont have account please signup";
		}
		return $result;
	}
	
	public function InsertUserss($data)
	{
		$firstname    = $data["firstName"];
		$username  = $data["username"];
		$password   = $data["password"];
		
		
		$salt = openssl_random_pseudo_bytes(32);
		$hexsalt = strtoupper(bin2hex($salt));

		$hash = hash('sha256', "$password" . "$hexsalt");
		$sql = "insert into users ( firstname, username, password, salt) 
		VALUES ('$firstname', '$username', ".'0x'.$hash.", ".'0x'.$hexsalt." )";
		$query = $this->db->query($sql);
		if($this->db->affected_rows() > '0')
		{
			$result["statusCode"] = "200";
			$result["status"] = 'success';
			$result["msg"] = "User Created Succesfully";
		}
		else {
			$result["statusCode"] = "400";
			$result["status"] = 'fail';
			$result["msg"] = "Error in creating user";
		}

		return $result;
	}

}
