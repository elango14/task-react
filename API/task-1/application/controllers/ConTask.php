<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConTask extends CI_Controller {
    
   	public function __construct() {
	    parent::__construct();
        $this->load->helper('url');
	    $this->load->model('ModTask');
   	}

	
	public function Login() 
	{
		header('Access-Control-Allow-Origin: *');
		header ("Access-Control-Allow-Headers: *");
		header('Content-Type: application/json');

		try {
			$data = json_decode(file_get_contents('php://input'), true);
			if(isset($data)) {
				$output = $this->ModTask->Loginn($data);
				echo json_encode($output);
			}
		}
		catch (exception $e) {
			echo $e->getMessage();
		}       
	}
	
	public function Register() 
	{
		header('Access-Control-Allow-Origin: *');
		header ("Access-Control-Allow-Headers: *");
		header('Content-Type: application/json');

		try {
			$data = json_decode(file_get_contents('php://input'), true);
			if(isset($data)) {
				$output = $this->ModTask->InsertUserss($data);
				echo json_encode($output);
			}
		}
		catch (exception $e) {
			echo $e->getMessage();
		}       
	}
}

?>



