export const userService = {
    login,
    logout,
    register,
};

var baseUrl = "https://appteq.in/react-task/ConTask/";

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    // call for API
    return fetch(`${baseUrl}Login`, requestOptions).then(handleResponse);
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

// register function
function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };
    
    // call for API
    return fetch(`${baseUrl}Register`, requestOptions).then(handleResponse);
}

// handle function of response
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            return true
        }
        return data;
    });
}