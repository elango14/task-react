import React, { useEffect } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { alert, buttons, jumbotron } from 'bootstrap-css'
import { useDispatch, useSelector } from 'react-redux';

import { history } from './_helpers';
import { alertActions } from './_actions';

import  LoginPage  from './component/loginPage/LoginPage';
import  RegisterPage  from './component/registerPage/RegisterPage';
import  DashboardPage  from './component/dashboard/DashboardPage.js';


function App() {
  
    const alert = useSelector(state => state.alert);
    const dispatch = useDispatch();

    useEffect(() => {
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
    }, []);

    return (
      <div styleName="jumbotron">
            <div className="container">
                <div className="col-md-8 offset-md-2">
                    {alert.message &&
                        <div className={`alert ${alert.type}`}>{alert.message}</div>
                    }
                    <BrowserRouter history={history}>
                        <Switch>
                            <Route exact path="/" component={LoginPage} />
                            <Route exact path="/register" component={RegisterPage} />
                            <Route exact path="/dashboard" component={DashboardPage} />
                        </Switch>
                    </BrowserRouter>
                </div>
            </div>
        </div>  
    );
}

export default App;
